<?php

$GLOBALS['TL_LANG']['tl_content']['left_text']   = array('Left column text', 'Text to go into the left column of this element.');
$GLOBALS['TL_LANG']['tl_content']['right_text']  = array('Right column text', 'Text to go into the right column of this element.');