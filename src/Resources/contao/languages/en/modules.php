<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Fields
 */
$GLOBALS['TL_LANG']['CTE']['boxlink'] = array('Box link', '');
$GLOBALS['TL_LANG']['CTE']['parallax_section'] = array('Parallax section', '');
$GLOBALS['TL_LANG']['CTE']['double_text']      = array('Text (2 columns)', '');


$GLOBALS['TL_LANG']['MOD']['meta_imex'] = array('Meta import/export', '');
$GLOBALS['TL_LANG']['MOD']['local_assets'] = array('Local assets','Define remote assets to save locally');