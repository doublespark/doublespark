Doublespark module
=================

Version 1.0.12 (2019-10-10)
--------------------------
## Updated
- Use new image code

Version 1.0.11 (2019-06-19)
--------------------------
## Updated
- Use excel formula instead of string counts in character count columns

Version 1.0.10 (2019-06-18)
--------------------------
## Feature
- Add character counts to export

Version 1.0.9 (2019-01-08)
--------------------------
## Fixed
- Support for contao manager

Version 1.0.8 (2019-01-08)
--------------------------
## Feature
- Add support for contao manager

Version 1.0.7 (2018-10-07)
--------------------------
## Feature
- Add ability to save and sync remote files to local disk

Version 1.0.6 (2017-10-11)
--------------------------
## Fixed
- Use page title for auto-alias

Version 1.0.5 (2017-09-01)
--------------------------
## Feature
- Enforce lowercase alias for news items and pages

## Fixed
- Removed reference to app_dev.php in import/export template

Version 1.0.4 (2017-08-18)
--------------------------
## Feature
- Remove trailing slashes from canonicals
- Correctly identify protocol

Version 1.0.3 (2017-07-17)
--------------------------
## Feature
- Add import/export feature

Version 1.0.2 (2017-05-09)
--------------------------
## Updated
- Change backend save button colour


Version 1.0.1 (2017-05-08)
--------------------------
## Fixed
- Don't extend contao core bundle


Version 1.0.0 (2017-05-08)
--------------------------
Initial version
